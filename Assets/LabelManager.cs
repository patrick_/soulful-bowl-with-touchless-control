﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class LabelManager : MonoBehaviour
{
    public TextMeshProUGUI _text;

    public void UpdateText(string text)
    {
        _text.text = text;
    }
}
