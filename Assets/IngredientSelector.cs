﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientSelector : MonoBehaviour
{
    public GameObject[] _ingredients;

    public GameObject _leftPalm, _rightPalm;

    private GestureManager _gestureManager;

    public CurvedLineRenderer _leftHandCurvedLineRenderer, _rightHandCurvedLineRenderer;

    public GameObject _leftHandSelectedIngredient = null, _rightHandSelectedIngredient = null;

    private GameObject _leftHandSelectedObject = null, _rightHandSelectedObject = null;
    private GameObject _leftHandSelectedObjectLastFrame = null, _rightHandSelectedObjectLastFrame = null;

    private float _leftHandLastTimeIngredientWasSelected = -1, _rightHandLastTimeIngredientWasSelected;

    private bool _leftHandSelectIsActive = true;
    private bool _rightHandSelectIsActive = true;

    public GameObject _leftLabel, _rightLabel;

    // Start is called before the first frame update
    void Start()
    {
        _gestureManager = GetComponent<GestureManager>();
    }

    public void LeftHandSelectionActive(bool isActive)
    {
        _leftHandSelectIsActive = isActive;
        _leftHandCurvedLineRenderer.gameObject.SetActive(isActive);
    }

    public void RightHandSelectionActive(bool isActive)
    {
        _rightHandSelectIsActive = isActive;
        _rightHandCurvedLineRenderer.gameObject.SetActive(isActive);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawLine(_leftPalm.transform.position, _leftPalm.transform.position + _leftPalm.transform.forward*2);

        Gizmos.color = Color.blue;

        for (int i = 0; i < _ingredients.Length; i++)
        {
            Vector3 handToObject = _ingredients[i].transform.position - _leftPalm.transform.position;
            Gizmos.DrawLine(_leftPalm.transform.position, _leftPalm.transform.position + handToObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        float selectionTimeInSeconds = 0.8f;

        bool leftHandPalmUpOpen = _gestureManager._leftHandCurGesture == GestureManager.Gesture.PalmUp_Open;
        bool rightHandPalmUpOpen = _gestureManager._rightHandCurGesture == GestureManager.Gesture.PalmUp_Open;

        if (leftHandPalmUpOpen && _leftHandSelectIsActive)
        {
            _leftHandCurvedLineRenderer.gameObject.SetActive(leftHandPalmUpOpen);
    
            int indexWithSmallestAngle = GetIndexOfClosestIngredient(_leftPalm.transform.position, _leftPalm.transform.forward);

            if (indexWithSmallestAngle != -1)
            {
                _leftHandLastTimeIngredientWasSelected = Time.time;
                _leftHandSelectedIngredient = _ingredients[indexWithSmallestAngle].GetComponent<IngredientPrefab>()._ingredientPrefab;
                _leftHandSelectedObject = _ingredients[indexWithSmallestAngle];

                _leftHandCurvedLineRenderer.UpdateLineRenderer(_leftPalm.transform.position, _leftPalm.transform.forward, _ingredients[indexWithSmallestAngle].transform.position);
            }
        } else
        {
            _leftHandCurvedLineRenderer.gameObject.SetActive(false);

            if (Time.time - _leftHandLastTimeIngredientWasSelected > selectionTimeInSeconds)
            {
                _leftHandSelectedIngredient = null;
                _leftHandSelectedObject = null;
            }
        }

        if (rightHandPalmUpOpen && _rightHandSelectIsActive)
        {
            _rightHandCurvedLineRenderer.gameObject.SetActive(rightHandPalmUpOpen);

            int indexWithSmallestAngle = GetIndexOfClosestIngredient(_rightPalm.transform.position, _rightPalm.transform.forward);

            if (indexWithSmallestAngle != -1)
            {
                _rightHandLastTimeIngredientWasSelected = Time.time;
                _rightHandSelectedIngredient = _ingredients[indexWithSmallestAngle].GetComponent<IngredientPrefab>()._ingredientPrefab;
                _rightHandSelectedObject = _ingredients[indexWithSmallestAngle];
                _rightHandCurvedLineRenderer.UpdateLineRenderer(_rightPalm.transform.position, _rightPalm.transform.forward, _ingredients[indexWithSmallestAngle].transform.position);
            }
        }
        else
        {
            _rightHandCurvedLineRenderer.gameObject.SetActive(false);

            if (Time.time - _rightHandLastTimeIngredientWasSelected > selectionTimeInSeconds)
            {
                _rightHandSelectedIngredient = null;
                _rightHandSelectedObject = null;
            }
        }


        //scale objects slightly
        float scale = 1.5f;

        //scale
        if (_leftHandSelectedObjectLastFrame !=_leftHandSelectedObject && _leftHandSelectedObject)
        {
            _leftHandSelectedObject.transform.localScale = new Vector3(scale, scale, scale);
        }

        if (_rightHandSelectedObjectLastFrame != _rightHandSelectedObject && _rightHandSelectedObject)
        {
            _rightHandSelectedObject.transform.localScale = new Vector3(scale, scale, scale);
        }


        //scale back
        if (_leftHandSelectedObjectLastFrame != _leftHandSelectedObject && _leftHandSelectedObjectLastFrame)
        {
            _leftHandSelectedObjectLastFrame.transform.localScale = new Vector3(1, 1, 1);
        }

        if (_rightHandSelectedObjectLastFrame != _rightHandSelectedObject && _rightHandSelectedObjectLastFrame)
        {
            _rightHandSelectedObjectLastFrame.transform.localScale = new Vector3(1, 1, 1);
        }

        //set labels
        _leftLabel.SetActive(_leftHandCurvedLineRenderer.gameObject.activeSelf);
        _rightLabel.SetActive(_rightHandCurvedLineRenderer.gameObject.activeSelf);

        if (_leftLabel.activeSelf)
        {
            _leftLabel.transform.position = _leftHandCurvedLineRenderer.linePositions[1];
            _leftLabel.GetComponent<LabelManager>().UpdateText(_leftHandSelectedIngredient.GetComponent<IngredientMetaInfo>()._ingredientName);
        }

        if (_rightLabel.activeSelf)
        {
            _rightLabel.transform.position = _rightHandCurvedLineRenderer.linePositions[1];
            _rightLabel.GetComponent<LabelManager>().UpdateText(_rightHandSelectedIngredient.GetComponent<IngredientMetaInfo>()._ingredientName);
        }


        _leftHandSelectedObjectLastFrame = _leftHandSelectedObject;
        _rightHandSelectedObjectLastFrame = _rightHandSelectedObject;
    }

    private int GetIndexOfClosestIngredient(Vector3 handPos, Vector3 handForward)
    {
        int indexWithSmallestAngle = 0;

        Vector3 handToObject = _ingredients[0].transform.position - handPos;
        float smallestAngle = Vector3.Angle(handToObject, handForward);

        for (int i = 0; i < _ingredients.Length; i++)
        {

            handToObject = _ingredients[i].transform.position - handPos;
            float newAngle = Vector3.Angle(handToObject, handForward);

            if (newAngle < smallestAngle)
            {
                indexWithSmallestAngle = i;
                smallestAngle = newAngle;
            }
        }

        return indexWithSmallestAngle;
    }
}
