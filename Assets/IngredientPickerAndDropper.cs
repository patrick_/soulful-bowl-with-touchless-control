﻿using NVIDIA.Flex;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientPickerAndDropper : MonoBehaviour
{
    private IngredientSelector _ingredientSelector;
    private GestureManager _gestureManager;

    private GameObject _leftHandPickedIngredient = null, _rightHandPickedIngredient = null;

    public Transform _leftPalm, _rightPalm;

    public OrderManager _orderManager;

    // Start is called before the first frame update
    void Start()
    {
        _ingredientSelector = GetComponent<IngredientSelector>();
        _gestureManager = GetComponent<GestureManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if(!_leftHandPickedIngredient && _gestureManager._leftHandCurGesture == GestureManager.Gesture.PalmUp_Close && _ingredientSelector._leftHandSelectedIngredient != null)
        {
            //create copy ingredient into left palm
            _leftHandPickedIngredient = GameObject.Instantiate(_ingredientSelector._leftHandSelectedIngredient,  _leftPalm.position, _ingredientSelector._leftHandSelectedIngredient.transform.rotation, _leftPalm);
            
            //Debug.Log("LH picked " + _ingredientSelector._leftHandSelectedIngredient.name);
            _ingredientSelector.LeftHandSelectionActive(false);
        }

        if (!_rightHandPickedIngredient && _gestureManager._rightHandCurGesture == GestureManager.Gesture.PalmUp_Close && _ingredientSelector._rightHandSelectedIngredient != null)
        {
            //create copy ingredient into right palm 
            _rightHandPickedIngredient = GameObject.Instantiate(_ingredientSelector._rightHandSelectedIngredient, _rightPalm.position, _ingredientSelector._rightHandSelectedIngredient.transform.rotation, _rightPalm);

            //Debug.Log("RH picked " + .name);
            _ingredientSelector.RightHandSelectionActive(false);
        }
    }

    public void DeleteLeftIngredient()
    {
        //Debug.Log("Drop Left");

        if (_leftHandPickedIngredient != null)
        {
 
            GameObject.DestroyImmediate(_leftHandPickedIngredient);
            _leftHandPickedIngredient = null;
            _ingredientSelector.LeftHandSelectionActive(true);
        }
    }

    public void DeleteRightIngredient()
    {
        if (_rightHandPickedIngredient != null)
        {
            GameObject.DestroyImmediate(_rightHandPickedIngredient);
            _rightHandPickedIngredient = null;
            _ingredientSelector.RightHandSelectionActive(true);
        }
    }

    public void CycleLeftIngredient()
    {
        if (_leftHandPickedIngredient && _leftHandPickedIngredient.GetComponent<IngredientMetaInfo>()._nextCyclePrefab)
        {
            GameObject oldIngredient = _leftHandPickedIngredient;
            GameObject newIngredient = _leftHandPickedIngredient.GetComponent<IngredientMetaInfo>()._nextCyclePrefab;

            _leftHandPickedIngredient = GameObject.Instantiate(newIngredient, _leftPalm.position, newIngredient.transform.rotation, _leftPalm);
            GameObject.DestroyImmediate(oldIngredient);
        }

    }

    public void CycleRightIngredient()
    {
        if (_rightHandPickedIngredient && _rightHandPickedIngredient.GetComponent<IngredientMetaInfo>()._nextCyclePrefab)
        {
            GameObject oldIngredient = _rightHandPickedIngredient;
            GameObject newIngredient = _rightHandPickedIngredient.GetComponent<IngredientMetaInfo>()._nextCyclePrefab;

            _rightHandPickedIngredient = GameObject.Instantiate(newIngredient, _rightPalm.position, newIngredient.transform.rotation, _rightPalm);
            GameObject.DestroyImmediate(oldIngredient);
        }
    }

    public void SpawnLeftIngredient()
    {
        if (_leftHandPickedIngredient)
        {
          
            GameObject newIngredient = GameObject.Instantiate(_leftHandPickedIngredient, _leftPalm.position, _leftHandPickedIngredient.transform.rotation);

            if (newIngredient.transform.childCount >=2 && newIngredient.transform.GetChild(1).name.CompareTo("Sauce_Oil") == 0)
            {
                GameObject.DestroyImmediate(newIngredient.transform.GetChild(1).gameObject);
            }

            FlexActor [] flexActors = newIngredient.GetComponentsInChildren<FlexActor>();

            foreach (var item in flexActors)
            {
                item.enabled = true;
            }

            FlexSoftSkinning[] flexSoftSkinning = newIngredient.GetComponentsInChildren<FlexSoftSkinning>();

            foreach (var item in flexSoftSkinning)
            {
                item.enabled = true;
            }


            var metaInfo = _leftHandPickedIngredient.GetComponent<IngredientMetaInfo>();
            _orderManager.AddIngredientToBowl(metaInfo._ingredientName, metaInfo._ingredientPrice);
        }
    }

    public void SpawnRightIngredient()
    {
        if (_rightHandPickedIngredient)
        {
            GameObject newIngredient = GameObject.Instantiate(_rightHandPickedIngredient, _rightPalm.position, _rightHandPickedIngredient.transform.rotation);

            if (newIngredient.transform.childCount >= 2 && newIngredient.transform.GetChild(1).name.CompareTo("Sauce_Oil") == 0)
            {
                GameObject.DestroyImmediate(newIngredient.transform.GetChild(1).gameObject);
            }

            FlexActor[] flexActors = newIngredient.GetComponentsInChildren<FlexActor>();

            foreach (var item in flexActors)
            {
                item.enabled = true;
            }

            var metaInfo = _rightHandPickedIngredient.GetComponent<IngredientMetaInfo>();
            _orderManager.AddIngredientToBowl(metaInfo._ingredientName, metaInfo._ingredientPrice);

        }
    }
}
