﻿using Leap;
using Leap.Unity;
using NVIDIA.Flex;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LMCDataFetcher : MonoBehaviour
{
    public LeapServiceProvider _leapServiceProvider;

    public FlexSourceActor [] _actors;
    private int _activeActorIndex = 0;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void IncrementActiveActorIndex()
    {
        _activeActorIndex = ++_activeActorIndex % _actors.Length;

        foreach (var item in _actors)
        {
            item.isActive = false;
        }

        Debug.Log("pinch, active: " + _activeActorIndex);
    }

    // Update is called once per frame
    void Update()
    {

        Frame currentFrame = _leapServiceProvider.CurrentFrame;

        List<Hand> hands = currentFrame.Hands;

        foreach (var hand in hands)
        {


            _actors[_activeActorIndex].isActive = hand.GrabAngle < 0.5f;
        }


    }
}
