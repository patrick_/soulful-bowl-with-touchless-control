﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IngredientMetaInfo : MonoBehaviour
{
    public GameObject _nextCyclePrefab;
    public string _ingredientName;
    public float _ingredientPrice;

}
