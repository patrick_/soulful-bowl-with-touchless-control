﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class OrderManager : MonoBehaviour
{
    public TextMeshProUGUI _totalPriceText;
    public Transform _ingredientUIRoot;
    public GameObject _ingredientUIPrefab;

    public GameObject _namePriceHeading;
    public ScrollRect _scrollRect;

    public GameObject _orderIsPrepared;

    public RectTransform _container = null;

    private float _totalPrice = 0f;

    //List<string> _ingredients;
    //List<float> _ingredientPrices;

    public void AddIngredientToBowl(string name, float price)
    {
        System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
        customCulture.NumberFormat.NumberDecimalSeparator = ".";
        System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;


        Debug.Log("add " + name + " " + price);

        var newIngredientUI = GameObject.Instantiate(_ingredientUIPrefab, _ingredientUIRoot);

        newIngredientUI.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = name;
        newIngredientUI.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = "$" + price.ToString("0.00");

        _totalPrice += price;

        _totalPriceText.text = "$" + _totalPrice.ToString("0.00");

        StartCoroutine(ScrollToBottom());
        //solution was to add contentSizefitter with vertical set to preferred size

       // _scrollRect.verticalNormalizedPosition = 0f;
    }

    public void PrepareOrder()
    {
        _scrollRect.gameObject.SetActive(false);
        _namePriceHeading.gameObject.SetActive(false);
        _orderIsPrepared.gameObject.SetActive(true);
    }


    IEnumerator ScrollToBottom()
    {
        yield return new WaitForEndOfFrame();
        //Canvas.ForceUpdateCanvases();
        _scrollRect.verticalNormalizedPosition = 0f;

        /*LayoutRebuilder.ForceRebuildLayoutImmediate(_container);
        _scrollRect.CalculateLayoutInputVertical();
        _scrollRect.CalculateLayoutInputVertical();
        _scrollRect.CalculateLayoutInputVertical();
        _scrollRect.CalculateLayoutInputVertical();
        yield return new WaitForEndOfFrame();
        _scrollRect.verticalNormalizedPosition = 0f;
        yield return new WaitForEndOfFrame();
        Canvas.ForceUpdateCanvases();
        yield return new WaitForEndOfFrame();
        LayoutRebuilder.ForceRebuildLayoutImmediate(_container);
        yield return new WaitForEndOfFrame();
        yield return new WaitForEndOfFrame();
        _scrollRect.verticalNormalizedPosition =  0f;*/

    }



}
