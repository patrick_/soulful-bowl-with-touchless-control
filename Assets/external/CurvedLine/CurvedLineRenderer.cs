﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent( typeof(LineRenderer) )]
public class CurvedLineRenderer : MonoBehaviour 
{
	//PUBLIC
	public float lineSegmentSize = 0.15f;
	public float lineWidth = 0.1f;

	public Vector3[] linePositions = new Vector3[3];

    public void UpdateLineRenderer(Vector3 startPos, Vector3 startDirection, Vector3 endPos)
    {

        Vector3 middlePos = startPos + startDirection * Vector3.Distance(startPos, endPos) * 0.5f;

        linePositions[0] = startPos;
        linePositions[1] = middlePos;
        linePositions[2] = endPos;

        LineRenderer line = this.GetComponent<LineRenderer>();

        //get smoothed values
        Vector3[] smoothedPoints = LineSmoother.SmoothLine(linePositions, lineSegmentSize);

        //set line settings
        line.positionCount = smoothedPoints.Length;
        line.SetPositions(smoothedPoints);
        line.startWidth = lineWidth;
        line.endWidth = lineWidth;
    }


}
