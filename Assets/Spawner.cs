﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{

    public GameObject _prefab;


    public int _countPerSpawn = 1;
    public float _spawnRateInSeconds = 1f;

    int _spawnedObjects = 0;
    int _localCounter = 0;

    private Vector3 _initPos;
    private Quaternion _initQuat;

    // Start is called before the first frame update
    void Start()
    {
        _initPos = _prefab.transform.position;
        _initQuat = _prefab.transform.rotation;

        StartCoroutine(SpawnObject());

    }

    IEnumerator SpawnObject()
    {
        while (true)
        {
            for (int i = 0; i < _countPerSpawn; i++)
            {
                //Vector3 randomPos = this.transform.position + Random.onUnitSphere * 0.2f;
                GameObject spawnedObject = GameObject.Instantiate(_prefab, _initPos, _initQuat, this.transform);
                //spawnedObject.transform.rotation = _initQuat;
                //spawnedObject.transform.position = _initPos;

                //Debug.Log(spawnedObject.transform.rotation);


                ++_spawnedObjects;
                ++_localCounter;
            }
            
            yield return new WaitForSeconds(_spawnRateInSeconds);



            if(_localCounter > 1000)
            {
                Debug.Log(_spawnedObjects);
                _localCounter = 0;
            }
        }




    }
}
