using UnityEngine;
using UnityEngine.Events;
using Leap;
using Leap.Unity;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class ShakeDetector : Detector {

    [Tooltip("The hand model to watch. Set automatically if detector is on a hand.")]
    public HandModelBase HandModel = null;
    //public float Period = .01f; //seconds
    
    private IEnumerator _watcherCoroutine;
    private IEnumerator _checkForShakeCoroutine;

    private float _waittimeInSecond = 0.25f;
    
    float[] _velocities;
    int _currentIndex = 0;
    bool canStart = false;

    // private Vector3 _lastVelocityVector;

    void Awake(){

        _velocities = new float[2 * 90];

      _watcherCoroutine = watcher();
      _checkForShakeCoroutine = shakeChecker();
    }

    void OnEnable () {
        StopCoroutine(_watcherCoroutine);
        StopCoroutine(_checkForShakeCoroutine);

        StartCoroutine(_watcherCoroutine);
        StartCoroutine(_checkForShakeCoroutine);
    }

    void OnDisable () {
      StopCoroutine(_watcherCoroutine);
    }


    IEnumerator shakeChecker()
    {
        while (true)
        {

            yield return new WaitForSeconds(_waittimeInSecond);

            List<float> velList = _velocities.ToList();
            velList.Sort();

            /*Debug.Log("Vels:");
            Debug.Log("smalles");
            Debug.Log(velList[1]);
            Debug.Log(velList[2]);
            Debug.Log(velList[3]);
            Debug.Log(velList[4]);

            Debug.Log("largest");
            Debug.Log(velList[velList.Count-1]);
            Debug.Log(velList[velList.Count - 2]);
            Debug.Log(velList[velList.Count - 3]);
            Debug.Log(velList[velList.Count - 4]);*/

            int centerIndex = (int)(velList.Count * 0.75f);

            if(velList[centerIndex] > 0.7f && velList[velList.Count - 1] > 0.9f)
            {
                Activate();

                for (int i = 0; i < _velocities.Length; i++) {
                    _velocities[i] = 0;
                }
                
            } else
            {
                Deactivate();
            }

            //Debug.Log("CenterVal at " + centerIndex + " " + velList[centerIndex] + " Last: " + velList[velList.Count-1]);

        }
    }


    IEnumerator watcher(){

      while(true){
            //Your logic to compute or check the current watchedValue goes here

            Hand hand = HandModel.GetComponent<RiggedHand>().GetLeapHand();

            if (hand != null)
            {
                _velocities[_currentIndex] = hand.PalmVelocity.Magnitude;       
            } else
            {
                _velocities[_currentIndex] = 0;
            }

            ++_currentIndex;

            if (_currentIndex >= _velocities.Length)
            {
                //Debug.Log("around");
                _currentIndex = 0;

            }

            yield return null; // new Wa WaitForSeconds(Period);
      }
    }
    
  }

