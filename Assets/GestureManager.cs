﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GestureManager : MonoBehaviour
{
    public enum Gesture
    {
        None,
        PalmUp_Open,
        PalmUp_Close,
        PalmDown_Open,
        PalmDown_Close,
        ThumbsUp
    }

    public Gesture _leftHandCurGesture = Gesture.None, _rightHandCurGesture = Gesture.None;

   

    // Start is called before the first frame update
    void Start()
    {
        
    }

    public void BothHandsThumbsUp()
    {
        _leftHandCurGesture = Gesture.ThumbsUp;
        _rightHandCurGesture = Gesture.ThumbsUp;

        Debug.Log("Thumbs Up");
    }


    public void Left_PalmUp_Open()
    {
        _leftHandCurGesture = Gesture.PalmUp_Open;

        
    }

    public void Left_None()
    {
        _leftHandCurGesture = Gesture.None;
        
    }

    public void Left_PalmDown_Open()
    {
        _leftHandCurGesture = Gesture.PalmDown_Open;

        //Debug.Log("Left_PalmDown_Open");
    }

    public void Left_PalmUp_Close()
    {
        _leftHandCurGesture = Gesture.PalmUp_Close;

        //Debug.Log("Left_PalmUp_Close");
    }

    public void Left_PalmDown_Close()
    {
        _leftHandCurGesture = Gesture.PalmDown_Close;
        //Debug.Log("Left_PalmDown_Close");
    }

    /// ///////////////////////////////////////////
    public void Right_PalmUp_Open()
    {
        _rightHandCurGesture = Gesture.PalmUp_Open;

        //Debug.Log("Right_PalmUp_Open");
    }

    public void Right_None()
    {
        _rightHandCurGesture = Gesture.None;
    }

    public void Right_PalmDown_Open()
    {
        _rightHandCurGesture = Gesture.PalmDown_Open;

        //Debug.Log("Right_PalmDown_Open");
    }

    public void Right_PalmUp_Close()
    {
        _rightHandCurGesture = Gesture.PalmUp_Close;

        //Debug.Log("Right_PalmUp_Close");
    }

    public void Right_PalmDown_Close()
    {
        _rightHandCurGesture = Gesture.PalmDown_Close;
        //Debug.Log("Right_PalmDown_Close");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
